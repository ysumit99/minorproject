<?php
require('config.php');

//echo "hi";

if(isset($_POST["submit_rating"]))
{
	$user_id = $_POST['user_id'];
	$movie_id = $_POST['movie_id'];
	$rating = $_POST['rating'];
	//echo $user_id;
	//echo $movie_id;
	//echo $rating;


	$query = "INSERT INTO rating(user_id, movie_id, rating) VALUES('$user_id','$movie_id','$rating')";
	$run_query = mysqli_query($con,$query);

	$query_userid = "SELECT * FROM  user_rating WHERE user_id = $user_id";
	$run_query_userid = mysqli_query($con,$query_userid);

	$query_movie = "SELECT * FROM movies WHERE id = $movie_id";
	$run_query_movie = mysqli_query($con,$query_movie);
	$row = mysqli_fetch_array($run_query_movie);

	




	if(mysqli_num_rows($run_query_userid) == 0)
	{
		$count_action = 0;
		$count_comedy = 0;
		$count_family = 0;
		$count_thriller = 0;
		$count_drama = 0;

		if($row['action'] == 1)
			$count_action++;
		if($row['comedy'] == 1)
			$count_comedy++;
		if($row['family'] == 1)
			$count_family++;
		if($row['thriller'] == 1)
			$count_thriller++;
		if($row['drama'] == 1)
			$count_drama++;

		if($count_action > 0)
			$avg_action = $rating;
		else
			$avg_action = 0;

		if($count_comedy > 0)
			$avg_comedy = $rating;
		else
			$avg_comedy = 0;

		if($count_family > 0)
			$avg_family = $rating;
		else
			$avg_family = 0;

		if($count_thriller > 0)
			$avg_thriller = $rating;
		else
			$avg_thriller = 0;

		if($count_drama > 0)
			$avg_drama = $rating;
		else
			$avg_drama = 0;


		//insert into user_rating table
		$query_insert_rating = "INSERT INTO user_rating(user_id, action_avg, comedy_avg, family_avg, drama_avg, thriller_avg, action_count, comedy_count, family_count, thriller_count, drama_count ) VALUES('$user_id','$avg_action','$avg_comedy', '$avg_family', '$avg_drama', '$avg_thriller', '$count_action', '$count_comedy', '$count_family', '$count_thriller', '$count_drama')";

		$run_query_insert_rating = mysqli_query($con,$query_insert_rating);




	}
	else
	{
		$row2 = mysqli_fetch_array($run_query_userid);
		$count_action = $row2['action_count'];
		$count_comedy = $row2['comedy_count'];
		$count_family = $row2['family_count'];
		$count_thriller = $row2['thriller_count'];
		$count_drama = $row2['drama_count'];

		$avg_action = $row2['action_avg'];
		$avg_comedy = $row2['comedy_avg'];
		$avg_family = $row2['family_avg'];
		$avg_thriller = $row2['thriller_avg'];
		$avg_drama = $row2['drama_avg'];

		if($row['action'] == 1)
		{
			$avg_action = (($count_action * $avg_action) + $rating)/($count_action+1);
			$count_action++;
		}
		if($row['comedy'] == 1)
		{
			$avg_comedy = (($count_comedy * $avg_comedy) + $rating)/($count_comedy+1);
			$count_comedy++;
		}
		if($row['family'] == 1)
		{
			$avg_family = (($count_family * $avg_family) + $rating)/($count_family+1);
			$count_family++;
		}
		if($row['thriller'] == 1)
		{
			$avg_thriller = (($count_thriller * $avg_thriller) + $rating)/($count_thriller+1);
			$count_thriller++;
		}
		if($row['drama'] == 1)
		{
			$avg_drama = (($count_drama * $avg_drama) + $rating)/($count_drama+1);
			$count_drama++;
		}

		//update avg rating query
		$query_update_rating = "UPDATE user_rating SET action_avg = $avg_action, comedy_avg = $avg_comedy, family_avg = $avg_family, drama_avg = $avg_drama, thriller_avg = $avg_thriller, action_count = $count_action, comedy_count = $count_comedy, family_count = $count_family, drama_count =  $count_drama, thriller_count = $count_thriller WHERE user_id = $user_id";
		$run_query_update_rating = mysqli_query($con,$query_update_rating);  




	}
}
else
{
	echo "error";
}

	
?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Recommendation system</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
  	.container{
  		background-color: #dcdcdc;
  	}
  </style>
		
	</head>
	<body>
		<div class="container">
			<form action="rate.php" method="POST">
				<div class="form-group col-md-4 col-md-offset-4">
					<label for="user_id">user id</label>
	    			<input type="text" class="form-control" id="user_id" name="user_id" placeholder="e.g 23">
	    			<br>

	    			<label for="movie_id">movie id</label>
	    			<input type="text" class="form-control" id="movie_id" name = "movie_id" placeholder="e.g 69">
	    			<br>

	    			<label for="rating">Rating</label>
	    			<input type="text" class="form-control" id="rating" name = "rating" placeholder="on a scale from 1 to 10">
	    			<br>

	    			<input type="submit" class="btn btn-success" name="submit_rating">

				</div>
			</form>
		</div>
		

			
		</div>

	</body>
	</html>