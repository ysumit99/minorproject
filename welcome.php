
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Recommendation system</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link rel="stylesheet" type="text/css" href="index.css">	

  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
  
	</head>
	<body>
		<!-- -->  

<nav class="navbar navbar-default">
<div class = "container">
  <div class = "navbar-header">
  	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target = "div1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
    <a href = "#" class = "navbar-brand"> <i class="fa fa-video-camera" aria-hidden="true"></i> MovieMerits </a>
    

  
  </div>

  <?php 
require('config.php');
session_start();
  if(isset($_SESSION['user_id'])) {
  $userLoggedIn = $_SESSION['user_id'];
  
 }
 else {
    header("Location: index.php");
 }
  if(isset($_POST['logout']))
  {
    
  session_destroy();
  header('location: index2.php?msg=Logged Out Successfully');
  }

   ?>

  <div class = "collapse navbar-collapse">
  <div class = "nav navbar-nav">
    
    <li>
      <a>
        <form method="GET" action="rate_new.php?id = $userLoggedIn">
        <input type="hidden" name="user_id" value="<?php echo $userLoggedIn ?>" />
        <input type = "submit" class="btn btn-primary" value="Rate movies">
    
      
      </form>
      </a>
    </li>

    <li>
      <a><button class="btn btn-success">
    Reviews
      </button>
      </a>
    </li>

    <li>
      <a><button class="btn btn-warning">
    Create Watchlist
      </button>
      </a>
    </li>

  

  </div>
  <div class = "nav navbar-nav navbar-right">
    <li><a ><?php echo "Welcome " . $userLoggedIn; ?></a></li>

   <li>
    <form method="POST">
      <input type="submit" class="btn btn-default navbar-btn btn-right mod1" value="logout" name="logout"></button>
    </form>
  </li>
   
  </div>

  <!-- <div class="modal fade" id="myModal" role="dialog"> 
    <div class="modal-dialog">-->
    
      <!-- Modal content 
      <div class="modal-content"> 
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">
          	Enter your Credentials    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          	<button class="btn btn-warning">
          		<li> <a href = "register.php"> New User? </a> </li>
          	</button>
          </h4>
        </div>
        <div class="modal-body">

          <form method="POST">
        	
			 <input type="text" class="form-control" placeholder="Username" name="user_id">
				
			 <br>
			
			 <input type="password" class="form-control" placeholder="Password" name="password">
			 <br>

			 <button class="btn btn-success btn-block" name="login">Login</button>

        </form>


         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"> <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Back</button>
        </div>
      </div>
      
    </div>
  </div> -->
  </div>

 	
  

</div>
</nav>


        <form action="search_movies.php" method="GET">

    
            <div class="col-md-6 col-md-offset-3">

              <div class="input-group">

                <input type="text" class="form-control" name = "query" placeholder="Search for Movies...">


                <span class="input-group-btn">
                  <input class="btn btn-default" type="submit" value="Go!">
                </span>

              </div>
            </div>

        </form>     
            
            <br>
            <br>

   



<div class="col-md-8">

	<div class="row">
	  <div class="col-xs-4 col-md-4">
	    <a href="#" class="thumbnail">
        <?php 
        $query_test = "SELECT * FROM movies WHERE id = 675";
        $run = mysqli_query($con,$query_test);
        $result123 = mysqli_fetch_array($run);
        $final_result = $result123['poster'];
        ?>
        <img src="<?php echo $final_result ?> "
         alt="">
      </a>
	  </div>

	  <div class="col-xs-4 col-md-4">
	   <a href="#" class="thumbnail">
        <?php 
        $query_test = "SELECT * FROM movies WHERE id = 18";
        $run = mysqli_query($con,$query_test);
        $result123 = mysqli_fetch_array($run);
        $final_result = $result123['poster'];
        ?>
        <img src="<?php echo $final_result ?> "
         alt="">
      </a>
	  </div>

	  <div class="col-xs-4 col-md-4">
	    <a href="#" class="thumbnail">
        <?php 
        $query_test = "SELECT * FROM movies WHERE id = 2";
        $run = mysqli_query($con,$query_test);
        $result123 = mysqli_fetch_array($run);
        $final_result = $result123['poster'];
        ?>
        <img src="<?php echo $final_result ?> "
         alt="">
      </a>
	  </div>

	  
	</div>

	<div class="row">
	  <div class="col-xs-4 col-md-4">
	   <a href="#" class="thumbnail">
        <?php 
        $query_test = "SELECT * FROM movies WHERE id = 171";
        $run = mysqli_query($con,$query_test);
        $result123 = mysqli_fetch_array($run);
        $final_result = $result123['poster'];
        ?>
        <img src="<?php echo $final_result ?> "
         alt="">
      </a>
	  </div>

	  <div class="col-xs-4 col-md-4">
	    <a href="#" class="thumbnail">
        <?php 
        $query_test = "SELECT * FROM movies WHERE id = 42";
        $run = mysqli_query($con,$query_test);
        $result123 = mysqli_fetch_array($run);
        $final_result = $result123['poster'];
        ?>
        <img src="<?php echo $final_result ?> "
         alt="">
      </a>
	  </div>

	  <div class="col-xs-4 col-md-4">
	    <a href="#" class="thumbnail">
	      <?php 
        $query_test = "SELECT * FROM movies WHERE id = 312";
        $run = mysqli_query($con,$query_test);
        $result123 = mysqli_fetch_array($run);
        $final_result = $result123['poster'];
        ?>
        <img src="<?php echo $final_result ?> "
         alt="">
	    </a>
	  </div>

	  
	</div>
  <?php

  $get_id_logged  = mysqli_query($con,"SELECT * FROM user WHERE user_name = '$userLoggedIn' ");


  while($get_id_logged_run = mysqli_fetch_array($get_id_logged))
    $id_finall = $get_id_logged_run['id'];
  //echo $id_finall;

  $query_content = mysqli_query($con,"SELECT * FROM user_rating WHERE user_id = $id_finall ");

  while($get_max = mysqli_fetch_array($query_content))
  {
    $max = $get_max['action_avg'];
    $flag = 1;

    if($get_max['comedy_avg'] > $max)
      {
        $max = $get_max['comedy_avg'];
        $flag = 2;
      }
    if($get_max['family_avg'] > $max)
      {
        $max = $get_max['family_avg'];
        $flag = 3;
      }
    if($get_max['drama_avg'] > $max)
      {
        $max = $get_max['drama_avg'];
        $flag = 4;
      }
    if($get_max['thriller_avg'] > $max)
      {
        $max = $get_max['thriller_avg'];
        $flag = 5;
      }

  }

  if($flag == 1)
  {
    $get_most_popular = mysqli_query($con,"SELECT * FROM movies WHERE action = 1 LIMIT 6");

  }
  else if($flag == 2)
  {
    $get_most_popular = mysqli_query($con,"SELECT * FROM movies WHERE comedy = 1 LIMIT 6");
  }
  else if($flag == 3)
  {
    $get_most_popular = mysqli_query($con,"SELECT * FROM movies WHERE family = 1 LIMIT 6");
  }
  else if($flag == 4)
  {
    $get_most_popular = mysqli_query($con,"SELECT * FROM movies WHERE drama = 1 LIMIT 6");
  }
  else if($flag == 5)
  {
    $get_most_popular = mysqli_query($con,"SELECT * FROM movies WHERE thriller = 1 LIMIT 6");
  }
  $flag2 = 0;
  $query_collab1 = mysqli_query($con,"SELECT * FROM cluster1 WHERE id = $id_finall ");
  $query_collab2 = mysqli_query($con,"SELECT * FROM cluster2 WHERE id = $id_finall ");
  $query_collab3 = mysqli_query($con,"SELECT * FROM cluster3 WHERE id = $id_finall ");
  $query_collab4 = mysqli_query($con,"SELECT * FROM cluster4 WHERE id = $id_finall ");
  $query_collab5 = mysqli_query($con,"SELECT * FROM cluster5 WHERE id = $id_finall ");
  if(mysqli_num_rows($query_collab1)>0)
  {
    //$get_movies = mysqli_query($con,"SELECT * FROM movies WHERE id = ")
    $flag2 = 1;
  }
  else if(mysqli_num_rows($query_collab2)>0)
  {
    $flag2 = 2;
  }
  
  else if(mysqli_num_rows($query_collab3)>0)
  {
    $flag2 = 3;
  }
  
  else if(mysqli_num_rows($query_collab4)>0)
  {
    $flag2 = 4;
  }
  
  else if(mysqli_num_rows($query_collab5)>0)
  {
    $flag2 = 5;
  }

  ?>

  <div class="row">
     <div class="well">
        <center><h4>Movies to Watch Next! (content based)</h4></center>

            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Movie</th>
                    <th>Year</th>
                    <th>Rating</th>
                  </tr>
                </thead>
                <tbody>
            <?php
            $count = 1;
             
            while($results = mysqli_fetch_array($get_most_popular)){
            // $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                          echo "<tr>
                <td> ".$count++."</td>
                <td>".$results['name']."</td>
                <td>".$results['year']."</td>
                <td>".$results['rating']."</td>

                


                </tr>";
                
                // posts results gotten from database(title and text) you can also show id ($results['id'])
            }

            ?>
            </tbody>
            </table>
        </div>
    
  </div>

  <div class="row">
     <div class="well">
        <center><h4>Movies to Watch Next! (based on collaborative filtering)</h4></center>

            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Movie</th>
                    <th>Year</th>
                    <th>Rating</th>
                  </tr>
                </thead>
                <tbody>
            <?php
            $count = 1;
             
             if($flag2 == 1)
             {
              $query_collab1 = mysqli_query($con,"SELECT * FROM cluster1 ");
              while($results = mysqli_fetch_array($query_collab1)){
              // $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                if($results['id'] != $id_finall)
                {
                  $which_movie = $results['id'];
                  $check_query_2 = mysqli_query($con,"SELECT * FROM rating WHERE user_id = $which_movie");
                  while($run_check_query_2 = mysqli_fetch_array($check_query_2))
                  {
                    $get_name = $run_check_query_2['movie_id'];
                    $run_get_name = mysqli_query($con,"SELECT * FROM movies WHERE id = $get_name");
                    while($result1234 = mysqli_fetch_array($run_get_name))
                    {
                      //$get_result1234 = $result1234['name'];
                      if($result1234['rating'] < 7)
                        continue;
                    
                            echo "<tr>
                  <td> ".$count++."</td>
                  <td>".$result1234['name']."</td>
                  <td>".$result1234['year']."</td>
                  <td>".$result1234['rating']."</td>

                  


                  </tr>";
                  }
                }
              }
                  
                  // posts results gotten from database(title and text) you can also show id ($results['id'])
              }
            }
            else if($flag2 == 2)
               {
                $query_collab2 = mysqli_query($con,"SELECT * FROM cluster2 ");
              while($results = mysqli_fetch_array($query_collab2)){
              // $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                if($results['id'] != $id_finall)
                {
                  $which_movie = $results['id'];
                  $check_query_2 = mysqli_query($con,"SELECT * FROM rating WHERE user_id = $which_movie");
                  while($run_check_query_2 = mysqli_fetch_array($check_query_2))
                  {
                    $get_name = $run_check_query_2['movie_id'];
                    $run_get_name = mysqli_query($con,"SELECT * FROM movies WHERE id = $get_name");
                    while($result1234 = mysqli_fetch_array($run_get_name))
                    {
                      //$get_result1234 = $result1234['name'];
                      if($result1234['rating'] < 7)
                        continue;
                    
                            echo "<tr>
                  <td> ".$count++."</td>
                  <td>".$result1234['name']."</td>
                  <td>".$result1234['year']."</td>
                  <td>".$result1234['rating']."</td>

                  


                  </tr>";
                  }
                }
              }
                  
                  // posts results gotten from database(title and text) you can also show id ($results['id'])
              }
            }
            else if($flag2 == 3)
               {
                $query_collab3 = mysqli_query($con,"SELECT * FROM cluster3 ");
              while($results = mysqli_fetch_array($query_collab3)){
              // $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                if($results['id'] != $id_finall)
                {
                  $which_movie = $results['id'];
                  $check_query_2 = mysqli_query($con,"SELECT * FROM rating WHERE user_id = $which_movie");
                  while($run_check_query_2 = mysqli_fetch_array($check_query_2))
                  {
                    $get_name = $run_check_query_2['movie_id'];
                    $run_get_name = mysqli_query($con,"SELECT * FROM movies WHERE id = $get_name");
                    while($result1234 = mysqli_fetch_array($run_get_name))
                    {
                      //$get_result1234 = $result1234['name'];
                      if($result1234['rating'] < 7)
                        continue;
                    
                            echo "<tr>
                  <td> ".$count++."</td>
                  <td>".$result1234['name']."</td>
                  <td>".$result1234['year']."</td>
                  <td>".$result1234['rating']."</td>

                  


                  </tr>";
                  }
                }
              }
                  
                  // posts results gotten from database(title and text) you can also show id ($results['id'])
              }
            }
            else if($flag2 == 4)
               {
                $query_collab4 = mysqli_query($con,"SELECT * FROM cluster4 ");
              while($results = mysqli_fetch_array($query_collab4)){
              // $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                if($results['id'] != $id_finall)
                {
                  $which_movie = $results['id'];
                  $check_query_2 = mysqli_query($con,"SELECT * FROM rating WHERE user_id = $which_movie");
                  while($run_check_query_2 = mysqli_fetch_array($check_query_2))
                  {
                    $get_name = $run_check_query_2['movie_id'];
                    $run_get_name = mysqli_query($con,"SELECT * FROM movies WHERE id = $get_name");
                    while($result1234 = mysqli_fetch_array($run_get_name))
                    {
                      //$get_result1234 = $result1234['name'];
                      if($result1234['rating'] < 7)
                        continue;
                    
                            echo "<tr>
                  <td> ".$count++."</td>
                  <td>".$result1234['name']."</td>
                  <td>".$result1234['year']."</td>
                  <td>".$result1234['rating']."</td>

                  


                  </tr>";
                  }
                }
              }
                  
                  // posts results gotten from database(title and text) you can also show id ($results['id'])
              }
            }
            else if($flag2 == 5)
               {
                $query_collab5 = mysqli_query($con,"SELECT * FROM cluster5 ");
              while($results = mysqli_fetch_array($query_collab5)){
              // $results = mysql_fetch_array($raw_results) puts data from database into array, while it's valid it does the loop
                if($results['id'] != $id_finall)
                {
                  $which_movie = $results['id'];
                  $check_query_2 = mysqli_query($con,"SELECT * FROM rating WHERE user_id = $which_movie");
                  while($run_check_query_2 = mysqli_fetch_array($check_query_2))
                  {
                    $get_name = $run_check_query_2['movie_id'];
                    $run_get_name = mysqli_query($con,"SELECT * FROM movies WHERE id = $get_name");
                    while($result1234 = mysqli_fetch_array($run_get_name))
                    {
                      //$get_result1234 = $result1234['name'];
                      if($result1234['rating'] < 7)
                        continue;
                    
                            echo "<tr>
                  <td> ".$count++."</td>
                  <td>".$result1234['name']."</td>
                  <td>".$result1234['year']."</td>
                  <td>".$result1234['rating']."</td>

                  


                  </tr>";
                  }
                }
              }
                  
                  // posts results gotten from database(title and text) you can also show id ($results['id'])
              }
            }

            ?>
            </tbody>
            </table>
        </div>
    
  </div>

</div>

<div class="col-md-4">
	<div class="well">
		<center><h4>Top Movies</h4></center>

		<table class="table table-striped">
    <thead>
      <tr>
        <th>#</th>
        <th>Movie</th>
        <th>Year</th>
        <th>Rating</th>
      </tr>
    </thead>
    <tbody>
      <!--<tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr>-->

<?php

  $query = "SELECT * FROM movies ORDER BY rating DESC LIMIT 10";
  $results=mysqli_query($con,$query);
//$row_count=mysql_num_rows($results);
  //$row_users = mysqli_fetch_array($results);
  $count = 1;
while ($row_users = mysqli_fetch_array($results)) {
    //output a row here
    echo "<tr>
    <td> ".$count++."</td>
    <td>".($row_users['name'])."</td>
    <td>".($row_users['year'])."</td>
    <td>".($row_users['rating'])."</td>


    </tr>";
}

?>
    </tbody>
  </table>
	</div>

	<div class="well">
		<center><h3>Critics</h3></center>
	</div>

</div>

	


	</body>
</html>