
<?php

require('config.php');

  $username = ""; 
  $password = "";
  $confirm_password = "";
  $email = "";
  $dob = "";
  $gender = "";
  $error_array = array();

if(isset($_POST['register']))
{
  reset($error_array);
  $username = strip_tags($_POST['username']);
  $password = strip_tags($_POST['password']);
  $confirm_password = strip_tags($_POST['confirm_password']);
  $email = strip_tags($_POST['email']);
  $dob = $_POST['dob'];
  $gender = $_POST['inlineRadioOptions'];

  /*echo $dob . '<br>' ;
  echo $username . '<br>';
  echo $password . '<br>';
  echo $confirm_password . '<br>';
  echo $email . '<br>';
  echo $gender . '<br>';*/




  if(filter_var($email,FILTER_VALIDATE_EMAIL)) {

      $email = filter_var($email,FILTER_VALIDATE_EMAIL);

      //check if email already exists

      $e_check = mysqli_query($con,"SELECT email FROM user WHERE email='$email'");

      //count the no. of rows returned

      $num_rows = mysqli_num_rows($e_check);

      if($num_rows > 0)
      {
        array_push($error_array, "This Email already in use<br>");
      }
    }
    else
    {
      array_push($error_array, "INAVLID FORMAT<br>");
    }

    $check_user_name = mysqli_query($con,"SELECT user_name FROM user WHERE user_name = '$username'");

     $num_rows_user = mysqli_num_rows($check_user_name);

      if($num_rows_user > 0)
      {
        array_push($error_array, "This username is already taken<br>");
      }

      if($password != $confirm_password) {
    array_push($error_array, "Your passwords dont match<br>");
  }

  if(empty($error_array)) {

    $password = md5($password);

  }

if(empty($error_array)) {

  $insert_query = mysqli_query($con,"INSERT INTO user VALUES (NULL,'$username','$password','$email','$gender','$dob')");
    header("Location: index.php");

  

}
/*else
{
  echo "NOT empty";
  for ($i=0; $i < count($error_array); $i++){

    echo current($error_array);
}

}*/

}
?>




<!DOCTYPE html>
<html>
<head>
	<title> Register </title>
	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Recommendation system</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="index.css">	

  
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<style>
	.div1
	{
		background-color: 	#FFFFE0;
		margin:2% 20% 20% 20%;
		padding:2% 1% 1% 7%;
	}

	h2
	{ 
		text-shadow: 2px 2px #ff0000;
		text-transform:uppercase;

	}

	.div2
	{
        margin-top:2%;
	}

	.div3
	{
		height:2em;
	}


</style>
</head>
<body>
  
  <div class = "jumbotron">
  	<div class = "container">
  	  <div class = "navbar-header">
        <a href = "#" class = "navbar-brand div3"> <i class="fa fa-video-camera" aria-hidden="true"></i> MovieMerits </a>
      </div>
      <h2 align = "center"> Enter Your Details </h2>
    </div>
  </div>
  
  <form class="form-horizontal div1" method="POST">
    <div class = "container">
      <div class="form-group">
        <label for="username" class="col-sm-2 control-label">Enter your username</label>
         <div class="col-sm-4">
         <input type="text" class="form-control" id="username" name="username" placeholder="eg. xyz">
         <span class="error">
              <?php if(in_array("This username is already taken<br>", $error_array))  echo "This username is already taken<br>";
             ?></span>
         </div>
      </div>

      <div class="form-group">
      <label for="inputPassword2" class="col-sm-2 control-label">Enter your password</label>
      <div class="col-sm-4">
      <input type="password" class="form-control" id="inputPassword2" name="password" placeholder="Password">
      <span class="error">
              <?php if(in_array("Your passwords dont match<br>", $error_array))  echo "Your passwords dont match<br>";
             ?></span>
      </div>
      </div>

      <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Confirm your password </label>
      <div class="col-sm-4">
      <input type="password" class="form-control" id="inputPassword3" name ="confirm_password" placeholder="Password">
      </div>
      </div>
    
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label"> Enter your Email</label>
         <div class="col-sm-4">
         <input type="email" class="form-control" id="inputEmail3" name = "email" placeholder="Email" >
         <span class="error">
              <?php if(in_array("This Email already in use<br>", $error_array))  echo "This Email already in use<br>";
             ?></span>
         </div>
      </div>

     
      <div class="form-group">
        <label for="date1" class="col-sm-2 control-label"> Enter your DOB</label>
        <div class="col-sm-4">
          <input type="date" class="form-control" id="date1" name="dob" placeholder="Date of Birth">
       </div>
     </div>

 

    
    <div class="form-group">
    <label for="age1" class="col-sm-2 control-label">Enter your gender</label>
    <div class="col-sm-4">
    <label class="radio-inline">
    <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="M"> Male
    </label>
    <label class="radio-inline">
    <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="F"> Female
    </label>
    <label class="radio-inline">
    <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="O"> Other
    </label>
    </div>
   
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input type="submit" class="btn btn-success btn-lg div2" name="register">
    </div>
  </div>
</form>
  </div>

</div>
</body>
</html>

