<?php
require('config.php');

$fetch_cluster = "SELECT * FROM  user_rating ";

$fetch_cluster_run = mysqli_query($con,$fetch_cluster);
	

$cluster = array( array('1' => '0' ,'2' => '0', '3' =>'0' ,'4' => '0', '5' => '0'));

$i = 0;

while($row = mysqli_fetch_array($fetch_cluster_run))
{
	$cluster[$i]['1'] = $row['action_avg'];
	//echo $cluster[$i]['1'] ;

	$cluster[$i]['2'] = $row['comedy_avg'];
	//echo $cluster[$i]['2'] ;

 	$cluster[$i]['3'] = $row['family_avg'];
	//echo $cluster[$i]['3'] ;

	$cluster[$i]['4'] = $row['drama_avg'];
	//echo $cluster[$i]['4'] ;

	$cluster[$i]['5'] = $row['thriller_avg'];
	//echo $cluster[$i]['5'] .'<br>';


	$i++;
}

$c1=1; $c2=1; $c3=1; $c4=1; $c5=1; // count of number of points in each cluster
//$centroid = array(array(),array(),array(),array(),array()); // contains the current centroid of each cluster
//$centroid1 = array(1,2,3,4,5);


$arr1 = array('1' => $cluster[0]['1'] ,'2' => $cluster[0]['2'] , '3' => $cluster[0]['3'] ,'4' => $cluster[0]['4'], '5' => $cluster[0]['5']);
$arr2 = array('1' => $cluster[1]['1'] ,'2' => $cluster[1]['2'] , '3' => $cluster[1]['3'] ,'4' => $cluster[1]['4'], '5' => $cluster[1]['5']);
$arr3 = array('1' => $cluster[2]['1'] ,'2' => $cluster[2]['2'] , '3' => $cluster[2]['3'] ,'4' => $cluster[2]['4'], '5' => $cluster[2]['5']);
$arr4 = array('1' => $cluster[3]['1'] ,'2' => $cluster[3]['2'] , '3' => $cluster[3]['3'] ,'4' => $cluster[3]['4'], '5' => $cluster[3]['5']);
$arr5 = array('1' => $cluster[4]['1'] ,'2' => $cluster[4]['2'] , '3' => $cluster[4]['3'] ,'4' => $cluster[4]['4'], '5' => $cluster[4]['5']);


$clus1 = array();

array_push($clus1, 0);

$clus2 = array();
array_push($clus2, 1);

$clus3 = array();
array_push($clus3, 2);

$clus4 = array();
array_push($clus4, 3);

$clus5 = array();
array_push($clus5, 4);



for($k = 0; $k < 10 ; $k++)
{


$flag = 0;

// creating cluster
for($i=5;$i<=24;$i++)
{
	if($k > 0 && $flag == 0)
		{
			$i = 0;
			$flag = 1;

		}
	//calculating distance to the 5 centroids

   $mod = sqrt($cluster[$i]['1']*$cluster[$i]['1'] + $cluster[$i]['2']*$cluster[$i]['2'] + $cluster[$i]['3']*$cluster[$i]['3'] + $cluster[$i]['4']*$cluster[$i]['4'] + $cluster[$i]['5']*$cluster[$i]['5']);
   $m = array($cluster[$i]['1']/$mod,$cluster[$i]['2']/$mod,$cluster[$i]['3']/$mod,$cluster[$i]['4']/$mod,$cluster[$i]['5']/$mod);
	  

	  // centroid 1
  $mod1 = sqrt($arr1['1']*$arr1['1'] + $arr1['2']*$arr1['2'] + $arr1['3']*$arr1['3']+ $arr1['4']*$arr1['4'] + $arr1['5']*$arr1['5']);

	$n1 = array($arr1['1']/$mod1,$arr1['2']/$mod1,$arr1['3']/$mod1,$arr1['4']/$mod1,$arr1['5']/$mod);
	$sum1 = $m[0]*$n1[0] + $m[1]*$n1[1] + $m[2]*$n1[2] + $m[3]*$n1[3] + $m[4]*$n1[4];
	echo "distance from centroid 1 = ". $sum1 . "<br>";//"for user " . $i+1 . " in iteration " . $k . " <br>"; 
	
	// centroid 2
  $mod2 = sqrt($arr2['1']*$arr2['1'] + $arr2['2']*$arr2['2'] + $arr2['3']*$arr2['3'] + $arr2['4']*$arr2['4'] + $arr2['5']*$arr2['5']);

	$n2 = array($arr2['1']/$mod2,$arr2['2']/$mod2,$arr2['3']/$mod2,$arr2['4']/$mod2,$arr2['5']/$mod2);
	
	$sum2 = $m[0]*$n2[0] + $m[1]*$n2[1] + $m[2]*$n2[2] + $m[3]*$n2[3] + $m[4]*$n2[4];

	//echo "distance from centroid 2 = ". $sum2 . "<br>";
   // centroid 3
  $mod3 = sqrt($arr3['1']*$arr3['1'] + $arr3['2']*$arr3['2'] + $arr3['3']*$arr3['3'] + $arr3['4']*$arr3['4'] + $arr3['5']*$arr3['5']);

	$n3 = array($arr3['1']/$mod3,$arr3['2']/$mod3,$arr3['3']/$mod3,$arr3['4']/$mod3,$arr3['5']/$mod3);
	
	$sum3 = $m[0]*$n3[0] + $m[1]*$n3[1] + $m[2]*$n3[2] + $m[3]*$n3[3] + $m[4]*$n3[4];

	//echo "distance from centroid 3 = ". $sum3 . "<br>";
   // centroid 4
  $mod4 = sqrt($arr4['1']*$arr4['1'] + $arr4['2']*$arr4['2'] + $arr4['3']*$arr4['3'] + $arr4['4']*$arr4['4'] + $arr4['5']*$arr4['5']);
	$n4 = array($arr4['1']/$mod4,$arr4['2']/$mod4,$arr4['3']/$mod4,$arr4['4']/$mod4,$arr4['5']/$mod4);
	
	$sum4 = $m[0]*$n4[0] + $m[1]*$n4[1] + $m[2]*$n4[2] + $m[3]*$n4[3] + $m[4]*$n4[4];

	//echo "distance from centroid 4 = ". $sum4 . "<br>";
  // centroid 5
$mod5 = sqrt($arr5['1']*$arr5['1'] + $arr5['2']*$arr5['2'] + $arr5['3']*$arr5['3'] + $arr5['4']*$arr5['4'] + $arr5['5']*$arr5['5']);
	$n5 = array($arr5['1']/$mod5,$arr5['2']/$mod5,$arr5['3']/$mod5,$arr5['4']/$mod5,$arr5['5']/$mod5);
	
	$sum5 = $m[0]*$n5[0] + $m[1]*$n5[1] + $m[2]*$n5[2] + $m[3]*$n5[3] + $m[4]*$n5[4];

	//echo "distance from centroid 5 = ". $sum5 . "<br>";
	$c = max($sum1,$sum2,$sum3,$sum4,$sum5);




// finding the cluster in which this current point must be added

	 if($c==$sum1)
	 {

	 		 	$c1++;
	 // putting the new point placed in this cluster inside the 'arr1' which contains all the points within this cluster
	 		 	$clus1[$c1-1] = $i;

	 		 	//echo $clus1[$c1-1]. "hi there<br>";
	 		 	

	 	// calculate new centroid for this cluster

	 	$x=0; $y=0; $z=0; $v=0; $w=0;
	 	
	 	for($j=0;$j<$c1;$j++){   // sums all the values of the points present inside the cluster
         
          $x = $x + $cluster[$clus1[$j]]['1'];
          $y = $y + $cluster[$clus1[$j]]['2'];
          $z = $z + $cluster[$clus1[$j]]['3'];
          $v = $v + $cluster[$clus1[$j]]['4'];
          $w = $w + $cluster[$clus1[$j]]['5'];

	 	}

   // new centroid
	 	$arr1['1'] = $x/$c1;
	 	$arr1['2'] = $y/$c1;
	 	$arr1['3'] = $z/$c1;
	 	$arr1['4'] = $v/$c1;
	 	$arr1['5'] = $w/$c1;

	 



	 }

	 

	  else if($c==$sum2)
	 {


	 		 	$c2++;
	 // putting the new point placed in this cluster inside the 'arr2' which contains all the points within this cluster
	 		 	//$clus2[$c2-1] = $i;
	 		 	array_push($clus2, $i);
	 		 	

	 	// calculate new centroid for this cluster
	 	
	 	$x=0; $y=0; $z=0; $v=0; $w=0;
	 	
	 	for($j=0;$j<$c2;$j++){   // sums all the values of the points present inside the cluster
         
         
         $temp = $clus2[$j];

          $x = $x + $cluster[$temp]['1'];
          $y = $y + $cluster[$clus2[$j]]['2'];
          $z = $z + $cluster[$clus2[$j]]['3'];
          $v = $v + $cluster[$clus2[$j]]['4'];
          $w = $w + $cluster[$clus2[$j]]['5'];

	 	}

   // new centroid
	 	$arr2['1'] = $x/$c2;
	 	$arr2['2'] = $y/$c2;
	 	$arr2['3'] = $z/$c2;
	 	$arr2['4'] = $v/$c2;
	 	$arr2['5'] = $w/$c2;


	 }



	 else if($c==$sum3)
	 {


	 		 	$c3++;
	 // putting the new point placed in this cluster inside the 'arr2' which contains all the points within this cluster
	 		 	$clus3[$c3-1] = $i;
	 		 	

	 	// calculate new centroid for this cluster
	 	
	 	$x=0; $y=0; $z=0; $v=0; $w=0;
	 	
	 	for($j=0;$j<$c3;$j++){   // sums all the values of the points present inside the cluster
         
          $x = $x + $cluster[$clus3[$j]]['1'];
          $y = $y + $cluster[$clus3[$j]]['2'];
          $z = $z + $cluster[$clus3[$j]]['3'];
          $v = $v + $cluster[$clus3[$j]]['4'];
          $w = $w + $cluster[$clus3[$j]]['5'];

	 	}

   // new centroid
	 	$arr3['1'] = $x/$c3;
	 	$arr3['2'] = $y/$c3;
	 	$arr3['3'] = $z/$c3;
	 	$arr3['4'] = $v/$c3;
	 	$arr3['5'] = $w/$c3;


	 }


	 else if($c==$sum4)
	 {


	 		 	$c4++;
	 // putting the new point placed in this cluster inside the 'arr2' which contains all the points within this cluster
	 		 	$clus4[$c4-1] = $i;
	 		 	

	 	// calculate new centroid for this cluster
	 	
	 	$x=0; $y=0; $z=0; $v=0; $w=0;
	 	
	 	for($j=0;$j<$c4;$j++){   // sums all the values of the points present inside the cluster
         
          $x = $x + $cluster[$clus4[$j]]['1'];
          $y = $y + $cluster[$clus4[$j]]['2'];
          $z = $z + $cluster[$clus4[$j]]['3'];
          $v = $v + $cluster[$clus4[$j]]['4'];
          $w = $w + $cluster[$clus4[$j]]['5'];

	 	}

   // new centroid
	 	$arr4['1'] = $x/$c4;
	 	$arr4['2'] = $y/$c4;
	 	$arr4['3'] = $z/$c4;
	 	$arr4['4'] = $v/$c4;
	 	$arr4['5'] = $w/$c4;

	 }


	 else if($c==$sum5)
	 {


	 		 	$c5++;
	 // putting the new point placed in this cluster inside the 'arr2' which contains all the points within this cluster
	 		 	$clus5[$c5-1] = $i;
	 		 	

	 	// calculate new centroid for this cluster
	 	
	 	$x=0; $y=0; $z=0; $v=0; $w=0;
	 	
	 	for($j=0;$j<$c5;$j++){   // sums all the values of the points present inside the cluster
         
          $x = $x + $cluster[$clus5[$j]]['1'];
          $y = $y + $cluster[$clus5[$j]]['2'];
          $z = $z + $cluster[$clus5[$j]]['3'];
          $v = $v + $cluster[$clus5[$j]]['4'];
          $w = $w + $cluster[$clus5[$j]]['5'];

	 	}

   // new centroid
	 	$arr5['1'] = $x/$c5;
	 	$arr5['2'] = $y/$c5;
	 	$arr5['3'] = $z/$c5;
	 	$arr5['4'] = $v/$c5;
	 	$arr5['5'] = $w/$c5;


	 }


}

for ($i=0; $i < $c1; $i++) { 
	echo $clus1[$i]+1 . " " ;
}

echo "<br>";

echo $arr1['1'] . " " . $arr1['2'] . " " . $arr1['3'] . " " . $arr1['4'] . " " . $arr1['5'] . "<br>";

for ($i=0; $i < $c2; $i++) { 
	echo $clus2[$i]+1 . " " ;
}

echo "<br>";

echo $arr2['1'] . " " . $arr2['2'] . " " . $arr2['3'] . " " . $arr2['4'] . " " . $arr2['5'] . "<br>";



for ($i=0; $i < $c3; $i++) { 
	echo $clus3[$i]+1 . " " ;
}

echo "<br>";
echo $arr3['1'] . " " . $arr3['2'] . " " . $arr3['3'] . " " . $arr3['4'] . " " . $arr3['5'] . "<br>";


for ($i=0; $i < $c4; $i++) { 
	echo $clus4[$i]+1 . " " ;
}

echo "<br>";
echo $arr4['1'] . " " . $arr4['2'] . " " . $arr4['3'] . " " . $arr4['4'] . " " . $arr4['5'] . "<br>";

for ($i=0; $i < $c5; $i++) { 
	echo $clus5[$i]+1 . " " ;
}

echo "<br>";
echo $arr5['1'] . " " . $arr5['2'] . " " . $arr5['3'] . " " . $arr5['4'] . " " . $arr5['5'] . "<br>";


//empty clus1,clus2.... and reinitialize c1,c2...
$c1 = 0; $c2 = 0; $c3 = 0; $c4 = 0; $c5 = 0;

unset($clus1); // $foo is gone
$clus1 = array();

unset($clus2); // $foo is gone
$clus2 = array();

unset($clus3); // $foo is gone
$clus3 = array();

unset($clus4); // $foo is gone
$clus4 = array();

unset($clus5); // $foo is gone
$clus5 = array();



echo "End of iteration". $k . "<br> <br> <br> <br>";
}








?>