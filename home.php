<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Recommendation system</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
	</head>
	<body>
		<!-- -->  

<nav class="navbar navbar-default">
  
  <button type="button" class="btn btn-default navbar-btn" data-toggle="modal" data-target="#myModal">Sign in</button>

  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">
          	Enter your Credentials &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          	<button class="btn btn-warning">
          		New User?
          	</button>
          </h4>
        </div>
        <div class="modal-body">
        	
			 <input type="text" class="form-control" placeholder="Username" >
				
			 <br>
			
			 <input type="password" class="form-control" placeholder="Password">
			 <br>

			 <button class="btn btn-success btn-block">Login</button>


         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"> <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>Back</button>
        </div>
      </div>
      
    </div>
  </div>

 	
  


</nav>

<div class="col-md-8">

	<div class="row">
	  <div class="col-xs-4 col-md-4">
	    <a href="#" class="thumbnail">
	      <img src="coder.jpg" alt="">
	    </a>
	  </div>

	  <div class="col-xs-4 col-md-4">
	    <a href="#" class="thumbnail">
	      <img src="coder.jpg" alt="">
	    </a>
	  </div>

	  <div class="col-xs-4 col-md-4">
	    <a href="#" class="thumbnail">
	      <img src="coder.jpg" alt="">
	    </a>
	  </div>

	  
	</div>

	<div class="row">
	  <div class="col-xs-4 col-md-4">
	    <a href="#" class="thumbnail">
	      <img src="coder.jpg" alt="">
	    </a>
	  </div>

	  <div class="col-xs-4 col-md-4">
	    <a href="#" class="thumbnail">
	      <img src="coder.jpg" alt="">
	    </a>
	  </div>

	  <div class="col-xs-4 col-md-4">
	    <a href="#" class="thumbnail">
	      <img src="coder.jpg" alt="">
	    </a>
	  </div>

	  
	</div>
</div>

<div class="col-md-4">
	<div class="well">
		<center><h4>Movie Rating</h4></center>

		<table class="table table-striped">
    <thead>
      <tr>
        <th>Movie</th>
        <th>Year</th>
        <th>Rating</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr>
    </tbody>
  </table>
	</div>

	<div class="well">
		<center><h3>Critics</h3></center>
	</div>

</div>

	


	</body>
</html>